import java.util.Date;

public class DemoObject {
    public static void main(String[] args) {
        System.out.println("Test");
        // kiểu dữ liệu (tên biến) = ( Khởi tạo / gán giá trị cho biến)
        // sư khac nhau giữa object và primitive : object khởi tạo giá trị , primitive gán giá trị
        Student student1 = new Student() ;// khởi tạo giá trị cho biến Student
//        student1.id = 1;
//        student1.msv= "230400";
//        student1.fullName=" phu son";
//        student1.birthDay = new Date();
        // bài toán : in ra msv của student1
        System.out.println(student1.msv);
        Student student2 = new Student() ;// khởi tạo giá trị cho biến Student
        student2.id = 2;
        student2.msv= "230403";
        student2.fullName=" ha nhi";
        student2.birthDay = new Date();
        float[] diemstudent2 = new float[3];
        diemstudent2[0] = 7;
        diemstudent2[1]= 7;
        diemstudent2[2]=8;
        student2.diem= diemstudent2 ; // c1; tạo 1 kiểu chứa các giá trị rồi gán
        student2.diem = new float[]{7.6f,8,5};// c2; gán trực tiếp


    }
}
