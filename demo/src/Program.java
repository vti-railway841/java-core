// java la mot ngon ngu huong đối tượng : có đặc điểm và có hành động )
// các đặc điểm...
// các hành động là các phương thức (method và hàm)
// - hàm main : dùng để chạy chương trình
// - và các method thường
public class Program { // kha nang truy cap class: muc do truy cap cao nhat . toan prj co the soi den dc
    // ben trong co the chua dc nhieu thu
    public static void main(String[] args) {
// nội dung code
        int number = 3;
        // 1. kiểu dữ liệu primitive ( dữ liệu nguyên thuỷ )
        // dạng logic : booleen( true, false)
        boolean logic = true;
        // kiểu dữ liệu dạng ký tự char ( giá trị là 1 kí tự trên bàn phím)
        char type3 = 'h';
        // kiểu dữ liệu số nguyên :byte, short,int, long
        byte number1 = 110; // chiếm 1byte
        short number2 = 5; // 2 byte
        int number3 = 10; // 4 byte
        long number4 = 2000; // 8 byte

        int number5 = 15;
        int number6 = number5 + number3;
        int number7 = type3 + number3;
        // kiểu dữ liệu số thực : double(8 byte) , float(4 byte)
        float number8 =7.8f;
        double number9 = 7.8;

        // 8 kiểu dữ liệu nguyên thuỷ( primitive)


        String chuoi1 = "hello";

        System.out.println( number9 );
    }
    // đây là method thường
    public void abc(){

    }
}

// buoi 2

