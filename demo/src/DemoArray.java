public class DemoArray {
    public static void main(String[] args) {
        // khai bao 1 danh sach int
        int [] numbers ={ 1,2,3};
//        System.out.println(numbers[2]);// in ra số ở vị trí số 2
//        System.out.println(numbers.length); // in ra số phần tử có trong danh sách
        float[] numbers2 = new float[5];// tạo 1 array có thể chứa tối đa 5 phần tử
        numbers2[0] = 1.1f; // gán giá trị cho các ô trống đó
        System.out.println(numbers2[0]);
    }
}
